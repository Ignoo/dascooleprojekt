package de.fastdata.hackathon.burgermacher;

import com.google.gson.annotations.SerializedName;

public class Burger {
    private Bestellung bestellung;
    private BurgerPatty burgerPatty;

    @Override
    public String toString() {
        return "Burger{" +
                "bestellung=" + bestellung +
                ", burgerPatty=" + burgerPatty +
                '}';
    }

    public Burger(Bestellung bestellung, BurgerPatty burgerPatty) {
        this.bestellung = bestellung;
        this.burgerPatty = burgerPatty;
    }

    public Bestellung getBestellung() {
        return bestellung;
    }

    public BurgerPatty getBurgerPatty() {
        return burgerPatty;
    }
}
