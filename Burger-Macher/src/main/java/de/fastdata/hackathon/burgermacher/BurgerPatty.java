package de.fastdata.hackathon.burgermacher;

public class BurgerPatty {
    private String pattyId;
    private String pattyType;

    public String getPattyId() {
        return pattyId;
    }

    public String getPattyType() {
        return pattyType;
    }
}
