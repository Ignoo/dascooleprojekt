package de.fastdata.hackathon.burgermacher;

import com.google.gson.annotations.SerializedName;

public class Bestellung {
    @SerializedName("BestellID")
    private int bestellId;
    @SerializedName("Vorname")
    private String vorname;
    @SerializedName("Nachname")
    private String nachname;
    @SerializedName("Bestellung")
    private String bestellung;

    public int getBestellId() {
        return bestellId;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public String getBestellung() {
        return bestellung;
    }
}