package de.fastdata.hackathon.producer;

import com.google.gson.Gson;
import de.fastdata.hackathon.burgermacher.Bestellung;
import de.fastdata.hackathon.burgermacher.BurgerPatty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.LinkedList;


/**
 * Sample Kafka consumer.
 *
 * @author Thomas Hamm
 * @version 0.1
 */
@Service
public class Consumer {

    public static LinkedList<Bestellung> bestellungen = new LinkedList<>();
    public static LinkedList<BurgerPatty> burgerPatties = new LinkedList<>();

    private final Logger logger = LoggerFactory.getLogger(Consumer.class);

    @KafkaListener(topics = "dcp-patty", groupId = "group_id_1")
    public void consumePatty(String message) {
        logger.info(String.format("Consumed message: %s", message));
        Gson gson = new Gson();
        BurgerPatty burgerPatty = gson.fromJson(message, BurgerPatty.class);
        burgerPatties.add(burgerPatty);
    }

    @KafkaListener(topics = "dcp-bestellung", groupId = "group_id_1")
    public void consumeBestellung(String message) {
        logger.info(String.format("Consumed message: %s", message));
        Gson gson = new Gson();
        Bestellung bestellung = gson.fromJson(message, Bestellung.class);
        bestellungen.add(bestellung);
    }
}
