package de.fastdata.hackathon.producer;

import com.google.gson.Gson;
import de.fastdata.hackathon.burgermacher.Burger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;


/**
 * Kafka producer service.
 *
 * @author Enrico Greyer
 * @version 0.1
 */
@Service
public class Producer {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void produceBurger(String topic, Burger burger) {
        logger.info("START produceBurger(), pattyId: '{}', bestellId'{}'", burger.getBurgerPatty().getPattyId(), burger.getBestellung().getBestellId());

        //convert burgerPatty to Json
        Gson gson = new Gson();
        String pattyString = gson.toJson(burger);

        this.kafkaTemplate.send(topic, pattyString);
        logger.info("END produceBurger(), pattyId: '{}', bestellId'{}'", burger.getBurgerPatty().getPattyId(), burger.getBestellung().getBestellId());
    }
}
