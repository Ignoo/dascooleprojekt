package de.fastdata.hackathon.producer;

import de.fastdata.hackathon.burgermacher.Burger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static de.fastdata.hackathon.consumer.Consumer.bestellungen;
import static de.fastdata.hackathon.consumer.Consumer.burgerPatties;


/**
 * Kafka producer application.
 *
 * @author Enrico Greyer
 * @version 0.1
 */
@SpringBootApplication
public class ProducerApplication implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(ProducerApplication.class);
    private static final String TOPIC = "dcp-burger";

    @Autowired
    private Producer producer;

    @Autowired
    private Consumer consumer;

    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        new Runnable() {
            @Override
            public void run() {
                while (true) {
                    logger.info("START producing burger");
                    if (bestellungen.isEmpty() || burgerPatties.isEmpty()) {
                        logger.info("one list is empty! bestellungen size {} patties size {}", bestellungen.size(), burgerPatties.size());
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        continue;
                    }
                    Burger burger = new Burger(bestellungen.getFirst(), burgerPatties.getFirst());
                    bestellungen.removeFirst();
                    burgerPatties.removeFirst();
                    producer.produceBurger(TOPIC, burger);
                    logger.info("burger produced: '{}'", burger.toString());
                    //wait for 5 seconds
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.run();


    }
}
