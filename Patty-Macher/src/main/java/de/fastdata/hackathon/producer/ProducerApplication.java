package de.fastdata.hackathon.producer;

import de.fastdata.hackathon.pattymacher.BurgerPatty;
import de.fastdata.hackathon.pattymacher.PattyType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Optional;


/**
 * Kafka producer application.
 *
 * @author Enrico Greyer
 * @version 0.1
 */
@SpringBootApplication
public class ProducerApplication implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(ProducerApplication.class);
    private static final String TOPIC = "dcp-patty";

    @Autowired
    private Producer producer;

    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        new Runnable() {
            @Override
            public void run() {
                while (true) {
                    logger.info("START produce new random patty");
                    int randomInteger = (int) (4 * Math.random());

                    //generate patty type
                    Optional<PattyType> pattyType = PattyType.fromId(randomInteger);
                    if(!pattyType.isPresent()){
                        logger.info("generator returned no valid patty type, skip patty construction");
                        continue;
                    }
                    String pattyTypeString = pattyType.get().toString();

                    //generate burger patty
                    BurgerPatty burgerPatty = new BurgerPatty(pattyTypeString);
                    logger.info("produce burger patty: '{}'", burgerPatty.toString());
                    producer.producePatty(TOPIC, burgerPatty);

                    //wait for 5 seconds
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.run();


    }
}
