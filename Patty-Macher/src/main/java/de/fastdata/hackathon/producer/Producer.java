package de.fastdata.hackathon.producer;

import com.google.gson.Gson;
import de.fastdata.hackathon.pattymacher.BurgerPatty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;


/**
 * Kafka producer service.
 *
 * @author Enrico Greyer
 * @version 0.1
 */
@Service
public class Producer {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    /**
     * Produces a BurgerPatty object as String to the kafka topic
     * @param topic the topic which gets the burgerpatty
     * @param burgerPatty the burgerpatty object
     */
    public void producePatty(String topic, BurgerPatty burgerPatty) {
        logger.info("START producePatty(), pattyId: '{}', pattyType'{}'", burgerPatty.getPattyId(), burgerPatty.getPattyTyp());

        //convert burgerPatty to Json
        Gson gson = new Gson();
        String pattyString = gson.toJson(burgerPatty);

        this.kafkaTemplate.send(topic, pattyString);
        logger.info("END producePatty(), pattyId: '{}'", burgerPatty.getPattyId());
    }
}
