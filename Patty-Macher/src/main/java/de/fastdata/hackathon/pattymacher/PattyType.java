package de.fastdata.hackathon.pattymacher;

import java.util.Optional;

/**
 * Enum for all kinds of meat or something similar ;)
 */
public enum PattyType {
    RIND(1),
    TOFU(2),
    HUHN(3);

    public int getTypeIdent() {
        return typeIdent;
    }

    private int typeIdent;
    private PattyType(int typeIdent){
        this.typeIdent = typeIdent;
    }

    public static Optional<PattyType> fromId(int typeIdent){
        for (PattyType type : values()) {
            if (type.getTypeIdent() == typeIdent) {
                return Optional.of(type);
            }
        }
        return Optional.empty();
    }

}
