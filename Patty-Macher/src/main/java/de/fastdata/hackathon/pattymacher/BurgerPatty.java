package de.fastdata.hackathon.pattymacher;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

/**
 * Burger Patty DTO
 */
public class BurgerPatty {
    @SerializedName("PattyId")
    private String pattyId = UUID.randomUUID().toString();
    @SerializedName("PattyTyp")
    private String pattyTyp;

    public BurgerPatty(String pattyTyp) {
        this.pattyTyp = pattyTyp;
    }

    public String getPattyId() {
        return pattyId;
    }

    public String getPattyTyp() {
        return pattyTyp;
    }

    @Override
    public String toString() {
        return "BurgerPatty{" +
                "pattyId='" + pattyId + '\'' +
                ", pattyTyp='" + pattyTyp + '\'' +
                '}';
    }
}
