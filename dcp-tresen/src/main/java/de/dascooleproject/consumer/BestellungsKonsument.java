package de.dascooleproject.consumer;

import com.google.gson.Gson;
import de.dascooleproject.consumer.pojo.Bestellung;
import de.dascooleproject.consumer.pojo.FertigeBestellung;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;


/**
 * This is a simple consumer of the orders, putting them in a threadpool waiting for the be finished
 * but handle orders asynchronously or they'll be waiting for each to finish
 *
 * @author keeyzar
 * @version 0.1
 */
@Service
public class BestellungsKonsument {

    private Gson gson = new Gson();
    private final Logger logger = LoggerFactory.getLogger(BestellungsKonsument.class);
    private final GerichtErsteller gerichtErsteller;

    @Autowired
    public BestellungsKonsument(GerichtErsteller gerichtErsteller) {
        this.gerichtErsteller = gerichtErsteller;
    }

    @KafkaListener(topics = "dcp-bestellung", groupId = "tresen-group")
    public void consume(String bestellungUnformatted) {
        Bestellung bestellung = gson.fromJson(bestellungUnformatted, Bestellung.class);
        logger.info(String.format("Consumed message: %s mit id %s", bestellung, bestellung.getBestellId()));
        try {
            gerichtErsteller.erstelleBestellung(bestellung);

        } catch (InterruptedException | ExecutionException e) {
            //Todo
            System.out.println("error");
            e.printStackTrace();
        }
        logger.info("bestellungskonsument ist fertig! %s", bestellung.getBestellId());
    }

}
