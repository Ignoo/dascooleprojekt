package de.dascooleproject.consumer;

import de.dascooleproject.consumer.pojo.Beilage;
import de.dascooleproject.consumer.pojo.Burger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Deque;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedDeque;


/**
 * basically the same as the burger consument, quite literally copy paste
 *
 * @author keeyzar
 * @version 0.1
 */
@Service
public class BeilagenKonsument {

    private final Logger logger = LoggerFactory.getLogger(BeilagenKonsument.class);
    private Deque<Beilage> beilagenQueue = new ConcurrentLinkedDeque<>();


    @KafkaListener(topics = "dcp-beilagen", groupId = "tresen-group")
    public void consume(Beilage beilage) {
        beilagenQueue.addFirst(beilage);
        logger.info(String.format("Consumed message: %s", beilage));
    }

    @Async("extern")
    public CompletableFuture<Beilage> getBeilage(int bestellId) throws InterruptedException {
        Optional<Beilage> beilage = Optional.of(new Beilage(0, "Salat", bestellId));
//        while(!(beilage = tryGetBurgerWithBestellId(bestellId)).isPresent()){
            Thread.sleep(5000);
//        }
        return CompletableFuture.completedFuture(beilage.get());
    }

    private Optional<Beilage> tryGetBurgerWithBestellId(int bestellId){
        return beilagenQueue.stream().filter((e) -> (e.getBestellId()) == bestellId).findFirst();
    }

}
