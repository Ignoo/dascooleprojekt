package de.dascooleproject.consumer.pojo;

import com.google.gson.annotations.SerializedName;

public class Burger {

    @SerializedName(value = "BurgerID")
    private int buergerId;

    @SerializedName(value = "BroetchenTyp")
    private String broetchenTyp;

    @SerializedName(value = "BestellID")
    private int bestellId;

    @SerializedName(value = "Patty")
    private String patty;

    public Burger(int buergerId, String broetchenTyp, int bestellId, String patty) {
        this.buergerId = buergerId;
        this.broetchenTyp = broetchenTyp;
        this.bestellId = bestellId;
        this.patty = patty;
    }

    public int getBuergerId() {
        return buergerId;
    }

    public void setBuergerId(int buergerId) {
        this.buergerId = buergerId;
    }

    public String getBroetchenTyp() {
        return broetchenTyp;
    }

    public void setBroetchenTyp(String broetchenTyp) {
        this.broetchenTyp = broetchenTyp;
    }

    public int getBestellId() {
        return bestellId;
    }

    public void setBestellId(int bestellId) {
        this.bestellId = bestellId;
    }

    public String getPatty() {
        return patty;
    }

    public void setPatty(String patty) {
        this.patty = patty;
    }
}
