package de.dascooleproject.consumer.pojo;

public class FertigeBestellung {
    private Bestellung bestellung;
    private Burger burger;
    private Beilage beilage;


    public FertigeBestellung(Bestellung bestellung) {
        this.bestellung = bestellung;
    }

    public void setBurger(Burger burger) {
        this.burger = burger;
    }

    public void setBeilage(Beilage beilage) {
        this.beilage = beilage;
    }

    public Bestellung getBestellung() {
        return bestellung;
    }

    public void setBestellung(Bestellung bestellung) {
        this.bestellung = bestellung;
    }

    public Burger getBurger() {
        return burger;
    }

    public Beilage getBeilage() {
        return beilage;
    }
}

/*

{

  "BestellID": 123,

  "Vorname": "Klaus",

  "Nachname": "Peter",

 "Bestellung": "Burger mit Pommes"

}
 */
