package de.dascooleproject.consumer.pojo;

import com.google.gson.annotations.SerializedName;

public class Bestellung {

    @SerializedName(value = "BestellID")
    private int bestellId;
    @SerializedName(value = "Vorname")
    private String vorname;
    @SerializedName(value = "Nachname")
    private String nachname;
    @SerializedName(value = "Bestellung")
    private String bestellung;

    public Bestellung(int bestellId, String vorname, String nachname, String bestellung) {
        this.bestellId = bestellId;
        this.vorname = vorname;
        this.nachname = nachname;
        this.bestellung = bestellung;
    }

    public int getBestellId() {
        return bestellId;
    }

    public void setBestellId(int bestellId) {
        this.bestellId = bestellId;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getBestellung() {
        return bestellung;
    }

    public void setBestellung(String bestellung) {
        this.bestellung = bestellung;
    }
}