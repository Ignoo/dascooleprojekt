package de.dascooleproject.consumer.pojo;

import com.google.gson.annotations.SerializedName;

/*
  "BeilagenID": 123,
  "BeilagenTyp": "Salat",
  "BestellID": 123
 */
public class Beilage {

    @SerializedName(value = "BeilagenID")
    private int beilagenId;

    @SerializedName(value = "BeilagenTyp")
    private String beilagenTyp;

    @SerializedName(value = "BestellID")
    private int bestellId;

    public Beilage(int beilagenId, String beilagenTyp, int bestellId) {
        this.beilagenId = beilagenId;
        this.beilagenTyp = beilagenTyp;
        this.bestellId = bestellId;
    }

    public int getBeilagenId() {
        return beilagenId;
    }

    public void setBeilagenId(int beilagenId) {
        this.beilagenId = beilagenId;
    }

    public String getBeilagenTyp() {
        return beilagenTyp;
    }

    public void setBeilagenTyp(String beilagenTyp) {
        this.beilagenTyp = beilagenTyp;
    }

    public int getBestellId() {
        return bestellId;
    }

    public void setBestellId(int bestellId) {
        this.bestellId = bestellId;
    }
}
