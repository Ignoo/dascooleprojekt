/**
 * create multiple consumers consuming
 * orders, their handling is done asynchronously, so the order taking is not halted, while
 * waiting for the ingredients to be finished.
 * both ingredients consumer handle putting 
 * @author keeyzar
 */
package de.dascooleproject.consumer;