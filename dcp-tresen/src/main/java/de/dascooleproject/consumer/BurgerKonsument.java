package de.dascooleproject.consumer;

import de.dascooleproject.consumer.pojo.Burger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Deque;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedDeque;


/**
 * Sample Kafka consumer.
 *
 * @author keeyzar
 * @version 0.1
 */
@Service
public class BurgerKonsument {

    private final Logger logger = LoggerFactory.getLogger(BurgerKonsument.class);
    /**
     * concurrent queue is necessary as multiple threads may access at once
     */
    private Deque<Burger> burgerQueue = new ConcurrentLinkedDeque<>();

    /**
     * just push all burgers into a queue
     * @param burger
     */
    @KafkaListener(topics = "dcp-burger", groupId = "tresen-group")
    public void consume(Burger burger) {
        burgerQueue.addFirst(burger);
        logger.info(String.format("Consumed message: %s", burger));
    }

    /**
     * handle get burger asynchrously, as the burger may take some time to arrive
     * the correct burger is identified by the bestellId
     */
    @Async("extern")
    public CompletableFuture<Burger> getBurger(int bestellId) throws InterruptedException {
        Optional<Burger> burger = Optional.of(new Burger(0, "korn", bestellId, "Rind"));
//        while(!(burger = tryGetBurgerWithBestellId(bestellId)).isPresent()){
            Thread.sleep(5000);
//        }
        return CompletableFuture.completedFuture(burger.get());
    }

    private Optional<Burger> tryGetBurgerWithBestellId(int bestellId){
        return burgerQueue.stream().filter((e) -> e.getBestellId() == bestellId).findFirst();
    }

}
