package de.dascooleproject.consumer;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;


/**
 * @Author: Kevin Kekule
 */
@SpringBootApplication
@EnableAsync
public class ConsumerApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
    }

    //we have three consumers acting at different times
    //first, the order checker, taking orders, an async one
    //waiting for 2 other async consumers to give back the correct ingredients for the order
    //finally putting them together
    //therefore, when multiple orders arrive at once, the order taker
    //may take all threads, causing a deadlock
    //therefore 2 thread pools are necessary, one for the order taker and
    //one for the both ingredients consumer
    @Bean(name="extern")
    public Executor taskExecutor() {
        //todo do stuff - lul - natürlich korrekt konfigurieren
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(3);
        executor.setMaxPoolSize(3);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("extern -- ");
        executor.initialize();
        return executor;
    }

    @Bean(name="gericht")
    public Executor taskExecutorTwo() {
        //todo do stuff - lul - natürlich korrekt konfigurieren
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(4);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("gericht --");
        executor.initialize();
        return executor;
    }


}
