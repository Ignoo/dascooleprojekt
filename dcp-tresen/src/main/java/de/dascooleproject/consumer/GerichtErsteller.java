package de.dascooleproject.consumer;

import de.dascooleproject.consumer.pojo.Beilage;
import de.dascooleproject.consumer.pojo.Bestellung;
import de.dascooleproject.consumer.pojo.Burger;
import de.dascooleproject.consumer.pojo.FertigeBestellung;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * this class handles orders asynchronously, so the order consumer can
 * put multiple orders on wait
 * @author keeyzar
 */
@Service
public class GerichtErsteller {
    private final Logger logger = LoggerFactory.getLogger(GerichtErsteller.class);

    private final BurgerKonsument burgerKonsument;
    private final BeilagenKonsument beilagenKonsument;

    @Autowired
    public GerichtErsteller(BurgerKonsument burgerKonsument, BeilagenKonsument beilagenKonsument) {
        this.burgerKonsument = burgerKonsument;
        this.beilagenKonsument = beilagenKonsument;
    }

    /**
     * handle order asynchronously, waiting for the ingredients, finally, when finished, printing it to the console
     * as the "display to the customer"
     */
    @Async("gericht")
    public void erstelleBestellung(Bestellung bestellung) throws InterruptedException, ExecutionException {
        logger.info("Hello wir wurden angehauen eine Bestllung anzufertigen! :)");
        FertigeBestellung fertigeBestellung = new FertigeBestellung(bestellung);
        int bestellId = fertigeBestellung.getBestellung().getBestellId();
        CompletableFuture<Burger> burger = burgerKonsument.getBurger(bestellId);
        CompletableFuture<Beilage> beilage = beilagenKonsument.getBeilage(bestellId);
        CompletableFuture.allOf(burger, beilage).join();
        fertigeBestellung.setBurger(burger.get());
        fertigeBestellung.setBeilage(beilage.get());
        printFertigeBestellung(fertigeBestellung);
    }

    private void printFertigeBestellung(FertigeBestellung fertigeBestellung) {

        String nachname = fertigeBestellung.getBestellung().getNachname();
        int bestellID = fertigeBestellung.getBestellung().getBestellId();
        Burger burger = fertigeBestellung.getBurger();
        Beilage beilage = fertigeBestellung.getBeilage();
        System.out.printf("\nHallo %s deine Bestellung mit der Nummer %s ist fertig.\n" +
                "Das ist ein %s-Burger und %s-Broetchen mit der Beilage: %s%n",
                nachname, bestellID, burger.getPatty(), burger.getBroetchenTyp(), beilage.getBeilagenTyp());
    }
}
