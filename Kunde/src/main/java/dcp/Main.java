package dcp;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Sample Kafka producer application.
 *
 * @author Thomas Hamm
 * @version 0.1
 */
@SpringBootApplication
public class Main implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    private static final String TOPIC = "fastdata-test-1";

    @Autowired
    private Producer producer;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        String jsonString;
        Gson gson = new Gson();
        while (true) {
            Thread.sleep(10000);
            jsonString = gson.toJson(BestellungsCreator.create());
            logger.info("Bestellung abgeschickt: "+jsonString);
            producer.sendMessage("dcp-bestellung", jsonString);
        }
    }
}
