package dcp;

import java.util.Random;

public class BestellungsCreator {

    private static int bestellid=1;
    private static String[] vornamen = {"Hans", "Kevin", "Basti", "Klaus", "Enrico"};
    private static String[] nachnamen = {"Meier", "Mueller", "Bauer", "Fischer"};
    private static String[] bestellungen = {"TOFU-Burger mit Pommes", "HUHN-Burger mit Salat", "RIND-Burger mit Chips"};

    public static Bestellung create() {
        return new Bestellung(bestellid++, getRandom(vornamen), getRandom(nachnamen), getRandom(bestellungen));
    }

    private static String getRandom(String[] array) {
        Random random = new Random();
        int length = array.length;
        int rand = random.nextInt(length);
        return array[rand];
    }
}
