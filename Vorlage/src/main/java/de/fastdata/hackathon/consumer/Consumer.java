package de.fastdata.hackathon.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;


/**
 * Sample Kafka consumer.
 *
 * @author Thomas Hamm
 * @version 0.1
 */
@Service
public class Consumer {

    private final Logger logger = LoggerFactory.getLogger(Consumer.class);

    @KafkaListener(topics = "fastdata-test-1", groupId = "group_id_1")
    public void consume(String message) {
        logger.info(String.format("Consumed message: %s", message));
    }
}
