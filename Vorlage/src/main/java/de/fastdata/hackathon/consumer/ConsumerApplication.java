package de.fastdata.hackathon.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Sample Kafka consumer application.
 *
 * @author Thomas Hamm
 * @version 0.1
 */
@SpringBootApplication
public class ConsumerApplication implements CommandLineRunner {

    private final Logger logger = LoggerFactory.getLogger(Consumer.class);

    @Autowired
    private Consumer consumer;

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
    }
}
