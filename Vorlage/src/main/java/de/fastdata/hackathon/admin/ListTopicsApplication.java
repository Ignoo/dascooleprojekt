package de.fastdata.hackathon.admin;

import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Sample Kafka topic management application.
 *
 * @author Thomas Hamm
 * @version 0.1
 */
@SpringBootApplication
public class ListTopicsApplication implements CommandLineRunner {

    private final Logger logger = LoggerFactory.getLogger(ListTopicsApplication.class);

    @Autowired
    private TopicManagement topicManagement;

    public static void main(String[] args) {
        SpringApplication.run(ListTopicsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {


        ListTopicsResult listTopicsResult = topicManagement.listTopics();
        logger.info(listTopicsResult.names().get().toString());


    }
}
