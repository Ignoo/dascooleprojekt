package de.fastdata.hackathon.admin;

import org.apache.kafka.clients.admin.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.stereotype.Service;

import java.util.Arrays;


/**
 * Sample Kafka topic management.
 *
 * @author Thomas Hamm
 * @version 0.1
 */
@Service
public class TopicManagement {

    private final Logger logger = LoggerFactory.getLogger(TopicManagement.class);

    private AdminClient adminClient;

    @Autowired
    public TopicManagement(KafkaAdmin admin) {
        adminClient = AdminClient.create(admin.getConfig());
    }

    public CreateTopicsResult createTopic(String name, int partitions, int replicationFactor) {
        return adminClient.createTopics(Arrays.asList(new NewTopic(name, partitions, (short) replicationFactor)));
    }

    public ListTopicsResult listTopics() {
        ListTopicsOptions listTopicsOptions = new ListTopicsOptions();
        listTopicsOptions.listInternal(true);
        return adminClient.listTopics(listTopicsOptions);
    }
}
