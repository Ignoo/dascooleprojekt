# Sample Kafka clients
Sample clients using Spring Kafka. Config for Confluent Cloud is already included.

Supported operations:
- Create/list topics
- Produce messages
- Consume messages